import ply.lex as lex

from ops import Skip


IGNORE = None

tokens = (
    'WHITESPACE',
    'NAME', 'LABEL',
    'DECIMAL', 'OCTAL', 'HEXADECIMAL', 'BINARY',
    'SKIP',
    'OPERAND',
    'REGISTER_OPERAND',
    'SECTION', 'ENDSECTION')

register_op = r'(r15|r14|r13|r12|r11|r10|r9|r8|r7|r6|r5|r4|r3|r2|r1|r0|lr|ip)'

cond_codes = [
    'eq',
    'neq',
    'c',
    'nc',
    'neg',
    'pos',
    'ov',
    'nov',
    'hi',
    'ls',
    'gte',
    'lt',
    'gt',
    'lte',
    'od',
    'ev',
]


def t_WHITESPACE(t):
    r'\s'
    return IGNORE


@lex.TOKEN(register_op)
def t_REGISTER_OPERAND(t):
    return t


@lex.TOKEN(r'(([-]?@|[-]?\#)?{rop})|((@|\#)?{rop}\+)'.format(rop=register_op))
def t_OPERAND(t):
    return t


@lex.TOKEN(r'skip([n]?z|{})'.format('|'.join(cond_codes)))
def t_SKIP(t):
    t.value = Skip(t.value)
    return t


def t_OCTAL(t):
    r'[-+]?0o[0-7]+'
    t.value = eval(t.value)
    return t


def t_BINARY(t):
    r'[-+]?0b[0-1]+'
    t.value = eval(t.value)
    return t


def t_HEXADECIMAL(t):
    r'[-+]?0x[0-9A-Fa-f]+'
    t.value = eval(t.value)
    return t


def t_DECIMAL(t):
    r'[-+]?\d+'
    t.value = int(t.value)
    return t


def t_LABEL(t):
    r'[a-zA-Z_]\w*\:'
    t.value = t.value.rstrip(':')
    return t


def t_SECTION(t):
    r'section'
    return t


def t_ENDSECTION(t):
    r'endsection'
    return t


def t_NAME(t):
    r'[a-zA-Z_]\w*'
    return t


def t_error(t):
    raise TypeError(f'Can not read {t.value}')


lex.lex()

lex.input("""
section text
blah001: 100
blah002: 0xdeadbeef 0o776 0b00001011
endsection

section code
blah003: load -#r14 r0
         skipneq
endsection
""")

for token in iter(lex.token, None):
    print(repr(token.type), repr(token.value))
