class Skip:
    base = 0b0110011101000000
    cond_codes = (
        'eq', 'neq', 'c', 'nc', 'neg', 'pos', 'ov', 'nov',
        'hi', 'ls', 'gte', 'lt', 'gt', 'lte', 'od', 'ev')
    mnemonic = None

    def __init__(self, mnemonic):
        self.mnemonic = mnemonic

    @property
    def condition(self) -> str:
        return self.mnemonic.lstrip('skip')

    @property
    def condition_code(self) -> int:
        cond = self.condition
        # handle condition aliases
        if cond == 'z':
            cond = 'eq'
        elif cond == 'nz':
            cond = 'neq'
        return self.cond_codes.index(cond)

    def __repr__(self) -> str:
        return self.mnemonic

    def opcode(self) -> int:
        return self.base | self.condition_code
